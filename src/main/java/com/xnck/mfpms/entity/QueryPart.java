/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.ManyMany;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

import java.util.List;

@Table("t_query_part")
public class QueryPart {

    public static final String FIELD_ID = "id";
    public static final String FIELD_QUERYNAME = "queryname";
    public static final String FIELD_PARTNAME = "partname";
    public static final String FIELD_DISPLAYNAME = "displayname";
    public static final String FIELD_RULES = "rules";

    @Name
    private String id;
    @Column
    private String queryname;
    @Column
    private String partname;
    @Column
    private String displayname;
    @Column
    private boolean enable;
    @ManyMany(target = RuleInfo.class, relation = "t_rule_query_part", from = "ruleid", to = "querypartid")
    private List<RuleInfo> rules;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQueryname() {
        return queryname;
    }

    public void setQueryname(String queryname) {
        this.queryname = queryname;
    }

    public String getPartname() {
        return partname;
    }

    public void setPartname(String partname) {
        this.partname = partname;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public List<RuleInfo> getRules() {
        return rules;
    }

    public void setRules(List<RuleInfo> rules) {
        this.rules = rules;
    }
}
