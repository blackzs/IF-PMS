/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.*;

import java.util.Date;
import java.util.List;

@Table("t_info_resource")
@View("v_info_resource")
public class ResourceInfo
{
    public static final String FIELD_ID = "id";
    public static final String FIELD_DISPLAYNAME = "displayname";
    public static final String FIELD_CODE = "code";
    public static final String FIELD_VIEWNAME = "viewname";
	public static final String FIELD_ENABLE = "enable";
    public static final String FIELD_ACTIONS = "actions";

	@Name
	private String id;
	@Column
	private String code;
	@Column
	private String displayname;
	@Column
	private String typeid;
	@Column
	private Date addtime;
	@Column
	private String remark;
	@Column
	private boolean enable;
	@Column
	private String createrid;
	@Column
	private String viewname;
    @Column
    @Readonly
    private String typename;
    @ManyMany(target = ActionInfo.class, relation = "t_action_resource", from = "actionid", to = "resourceid")
    private List<ActionInfo> actions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getCreaterid() {
        return createrid;
    }

    public void setCreaterid(String createrid) {
        this.createrid = createrid;
    }

    public String getViewname() {
        return viewname;
    }

    public void setViewname(String viewname) {
        this.viewname = viewname;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public List<ActionInfo> getActions() {
        return actions;
    }

    public void setActions(List<ActionInfo> actions) {
        this.actions = actions;
    }
}