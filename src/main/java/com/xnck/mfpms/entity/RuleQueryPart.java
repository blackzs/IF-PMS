/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

@Table("t_rule_query_part")
@PK({"ruleid", "querypartid"})
public class RuleQueryPart {

    public static final String FIELD_RULEID = "ruleid";
    public static final String FIELD_QUERYPARTID = "querypartid";

    private String ruleid;
    private String querypartid;

    public String getRuleid() {
        return ruleid;
    }

    public void setRuleid(String ruleid) {
        this.ruleid = ruleid;
    }

    public String getQuerypartid() {
        return querypartid;
    }

    public void setQuerypartid(String querypartid) {
        this.querypartid = querypartid;
    }
}
