package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

@Table("t_rule_child")
@PK({"parentid", "childid"})
public class RuleChild {
    public static final String FIELD_CHILDID = "childid";
    public static final String FIELD_PARENTID = "parentid";

    private String parentid;
    private String childid;

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getChildid() {
        return childid;
    }

    public void setChildid(String childid) {
        this.childid = childid;
    }
}
