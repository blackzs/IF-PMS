/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

@Table("t_action_resource")
@PK({"actionid", "resourceid"})
public class ActionResource
{

    public static final String FIELD_ACTIONID = "actionid";
    public static final String FIELD_RESOURCEID = "resourceid";

	private String actionid;
	private String resourceid;

	public String getActionid()
	{
		return actionid;
	}
	public void setActionid(String actionid)
	{
		this.actionid=actionid;
	}
	public String getResourceid()
	{
		return resourceid;
	}
	public void setResourceid(String resourceid)
	{
		this.resourceid=resourceid;
	}

}