/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

@Table("t_user_action")
@PK({"userid","actionid"})
public class UserAction
{
    public static final String FIELD_USERID = "userid";
	public static final String FIELD_ACTIONID = "actionid";

	private String userid;
	private String actionid;

    public String getUserid()
	{
		return userid;
	}
	public void setUserid(String userid)
	{
		this.userid=userid;
	}
	public String getActionid()
	{
		return actionid;
	}
	public void setActionid(String actionid)
	{
		this.actionid=actionid;
	}

}