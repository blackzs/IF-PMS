/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

import java.util.Date;

@Table("t_info_company")
public class CompanyInfo
{
	@Name
	private String id;
	@Column
	private String shortname;
	@Column
	private String displayname;
	@Column
	private String email;
	@Column
	private String tel;
	@Column
	private String linkman;
	@Column
	private Date createtime;
	@Column
	private String creatorid;

	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id=id;
	}
	public String getShortname()
	{
		return shortname;
	}
	public void setShortname(String shortname)
	{
		this.shortname=shortname;
	}
	public String getDisplayname()
	{
		return displayname;
	}
	public void setDisplayname(String displayname)
	{
		this.displayname=displayname;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email=email;
	}
	public String getTel()
	{
		return tel;
	}
	public void setTel(String tel)
	{
		this.tel=tel;
	}
	public String getLinkman()
	{
		return linkman;
	}
	public void setLinkman(String linkman)
	{
		this.linkman=linkman;
	}
	public Date getCreatetime()
	{
		return createtime;
	}
	public void setCreatetime(Date createtime)
	{
		this.createtime=createtime;
	}

	public String getCreatorid() {
		return creatorid;
	}

	public void setCreatorid(String creatorid) {
		this.creatorid = creatorid;
	}
}