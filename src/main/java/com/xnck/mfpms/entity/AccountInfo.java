/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.One;
import org.nutz.dao.entity.annotation.Table;

import java.util.Date;

@Table("t_info_account")
public class AccountInfo
{
    public static final String FIELD_USERID = "userid";
    public static final String FIELD_LOGINNAME = "loginname";
    public static final String FIELD_USER = "user";

	@Name
	private String id;
	@Column
	private String loginname;
	@Column
	private String loginpwd;
	@Column
	private String userid;
	@Column
	private Date createtime;
    @One(target = UserInfo.class, field = "userid")
    private UserInfo user;

	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id=id;
	}
	public String getLoginname()
	{
		return loginname;
	}
	public void setLoginname(String loginname)
	{
		this.loginname=loginname;
	}
	public String getLoginpwd()
	{
		return loginpwd;
	}
	public void setLoginpwd(String loginpwd)
	{
		this.loginpwd=loginpwd;
	}
	public String getUserid()
	{
		return userid;
	}
	public void setUserid(String userid)
	{
		this.userid=userid;
	}
	public Date getCreatetime()
	{
		return createtime;
	}
	public void setCreatetime(Date createtime)
	{
		this.createtime=createtime;
	}

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }
}