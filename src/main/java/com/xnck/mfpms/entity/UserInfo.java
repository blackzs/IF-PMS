/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.*;

import java.util.Date;
import java.util.List;

@Table("t_info_user")
@View("v_info_user")
public class UserInfo
{
	public static final String FIELD_ID = "id";
	public static final String FIELD_DISPLAYNAME = "displayname";
    public static final String FIELD_COMPANY = "company";
    public static final String FIELD_ROLES = "roles";
    public static final String FIELD_MENUS = "menus";
    public static final String FIELD_ACTIONS = "actions";
    public static final String FIELD_RULES = "rules";

	@Name
	private String id;
	@Column
	private String displayname;
	@Column
	private boolean enable;
	@Column
	private Date addtime;
	@Column
	private String createrid;
	@Column
	private String name;
	@Column
    private String email;
	@Column
    private String companyid;
    @Column
    @Readonly
    private String companyname;
    @Column
    @Readonly
    private String companyshortname;
    @One(target = CompanyInfo.class, field = "companyid")
    private CompanyInfo company;
	@ManyMany(target = RoleInfo.class, relation = "t_user_role", from = "userid", to = "roleid")
	private List<RoleInfo> roles;
    @ManyMany(target = MenuInfo.class, relation = "t_user_menu", from = "userid", to = "menuid")
    private List<MenuInfo> menus;
    @ManyMany(target = ActionInfo.class, relation = "t_user_action", from = "userid", to = "actionid")
    private List<ActionInfo> actions;
    @ManyMany(target = RuleInfo.class, relation = "t_user_rule", from = "userid", to = "ruleid")
    private List<RuleInfo> rules;

	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id=id;
	}
	public String getDisplayname()
	{
		return displayname;
	}
	public void setDisplayname(String displayname)
	{
		this.displayname=displayname;
	}
	public Date getAddtime()
	{
		return addtime;
	}
	public void setAddtime(Date addtime)
	{
		this.addtime=addtime;
	}
	public String getCreaterid()
	{
		return createrid;
	}
	public void setCreaterid(String createrid)
	{
		this.createrid=createrid;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name=name;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCompanyshortname() {
        return companyshortname;
    }

    public void setCompanyshortname(String companyshortname) {
        this.companyshortname = companyshortname;
    }

    public CompanyInfo getCompany() {
        return company;
    }

    public void setCompany(CompanyInfo company) {
        this.company = company;
    }

    public List<RoleInfo> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleInfo> roles) {
        this.roles = roles;
    }

    public List<MenuInfo> getMenus() {
        return menus;
    }

    public void setMenus(List<MenuInfo> menus) {
        this.menus = menus;
    }

    public List<ActionInfo> getActions() {
        return actions;
    }

    public void setActions(List<ActionInfo> actions) {
        this.actions = actions;
    }

    public List<RuleInfo> getRules() {
        return rules;
    }

    public void setRules(List<RuleInfo> rules) {
        this.rules = rules;
    }
}