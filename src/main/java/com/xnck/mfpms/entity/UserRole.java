/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.entity;

import org.nutz.dao.entity.annotation.PK;
import org.nutz.dao.entity.annotation.Table;

@Table("t_user_role")
@PK({"roleid","userid"})
public class UserRole
{
	public static final String FIELD_USERID = "userid";
	public static final String FIELD_ROLEID = "roleid";

	private String roleid;
	private String userid;

    public String getRoleid()
	{
		return roleid;
	}
	public void setRoleid(String roleid)
	{
		this.roleid=roleid;
	}
	public String getUserid()
	{
		return userid;
	}
	public void setUserid(String userid)
	{
		this.userid=userid;
	}

}