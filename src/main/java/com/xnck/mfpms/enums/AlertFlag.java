/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.enums;

public enum AlertFlag {
    TIME_OUT,
    SESSION_OUT,
    AUTHEN_FAIL,
    PERMISS_FAIL
}
