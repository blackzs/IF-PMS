/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.util;

import com.xnck.mfpms.constant.ContextConstant;

import javax.servlet.http.HttpServletRequest;

;

/**
 * Http Session常用工具类
 * @author zhangmengliang
 *
 */
public class SessionUtils {
	/**
     * 设置session的值
     * 
     * @param request
     * @param key
     * @param value
     */
    public static void setAttr(HttpServletRequest request, String key, Object value) {
        request.getSession(true).setAttribute(key, value);
    }

    /**
     * 获取session的值
     * 
     * @param request
     * @param key
     */
    public static Object getAttr(HttpServletRequest request, String key) {
        return request.getSession(true).getAttribute(key);
    }

    /**
     * 删除Session值
     * 
     * @param request
     * @param key
     */
    public static void removeAttr(HttpServletRequest request, String key) {
        request.getSession(true).removeAttribute(key);
    }
    
    /**
     * 获得当前登录人员的凭证信息
     * @param request
     * @return
     * @throws Exception
     * @author:zhangmengliang
     * @date: 2015年10月31日
     */
    public static String getCurrentUserId(HttpServletRequest request) throws Exception{
    	Object obj = getAttr(request, ContextConstant.IDEN_CERT_KEY);
		if (null == obj) {
			return null;
		}
    	return obj.toString();
    }

    /**
     * 设置当前登录人员的凭证信息
     * @param request
     * @param value
     * @throws Exception
     */
    public static void setCurrentUserId(HttpServletRequest request, String value) throws Exception{
        setAttr(request, ContextConstant.IDEN_CERT_KEY, value);
    }
}
