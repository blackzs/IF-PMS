/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.util;


import com.xiaoleilu.hutool.DateUtil;
import com.xiaoleilu.hutool.Validator;

import java.util.regex.Pattern;

public class ValidatorUtils extends Validator {

    /** 移动电话 */
    public final static Pattern TEL = Pattern.compile("^(?:(?:0\\d{2,3}[\\- ]?[1-9]\\d{6,7})|(?:[48]00[\\- ]?[1-9]\\d{6}))$");
    public final static Pattern NEW_EMAIL = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
    /**
     * 判断是否是日期时间类型
     * @param value
     * @return
     */
    public static boolean isDateTime(String value){
        try {
            DateUtil.parse(value);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static boolean isTel(String value){
        return isByRegex(TEL,value);
    }

    public static boolean isNewEmail(String value){
        return isByRegex(NEW_EMAIL, value);
    }
}
