/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.util;

import com.xiaoleilu.hutool.StrUtil;
import org.nutz.dao.sql.Criteria;

public class PagerUtil {

    public static int getCurrentPage(int offset, int limit){
        if (0 == offset){
            return 1;
        }
        return offset / limit + 1;
    }

    public static void getSearchOrder(Criteria cri, String defaultField, String orderName, String orderType){
        if (StrUtil.isNotBlank(orderName)){
            if ("desc".equals(orderType.toLowerCase())){
                cri.getOrderBy().desc(orderName);
            }
            else {
                cri.getOrderBy().asc(orderName);
            }
        }
        else if(StrUtil.isNotBlank(defaultField)) {
            cri.getOrderBy().asc(defaultField);
        }
    }
}
