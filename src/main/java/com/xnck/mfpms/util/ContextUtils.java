/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.util;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 常用工具类
 * @author zhangmengliang
 *
 */
public class ContextUtils {
	/**
	 * response输出字符串
	 * @param response
	 * @param str
	 * @throws Exception
	 * @author:zhangmengliang
	 * @date: 2015年11月4日
	 */
	public static void responseString(HttpServletResponse response, String str) throws Exception{
		try {
			//设置页面不缓存
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setCharacterEncoding("utf-8");
			PrintWriter out= null;
			out = response.getWriter();
			out.print(str);
			out.flush();
			out.close();
		} catch (Exception e) {
			throw e;
		}
	} 
}
