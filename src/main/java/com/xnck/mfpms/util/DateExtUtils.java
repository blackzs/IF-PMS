/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.util;

import com.xiaoleilu.hutool.DateTime;
import com.xiaoleilu.hutool.DateUtil;

import java.util.Calendar;
import java.util.Date;

public class DateExtUtils extends DateUtil {

    /**
     * 获取某天的开始时间
     * @param date 日期
     * @return 某天的开始时间
     */
    public static DateTime getBeginTimeOfDay(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return new DateTime(calendar.getTime());
    }

    /**
     * 获取某天的结束时间
     * @param date 日期
     * @return 某天的结束时间
     */
    public static DateTime getEndTimeOfDay(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return new DateTime(calendar.getTime());
    }
}
