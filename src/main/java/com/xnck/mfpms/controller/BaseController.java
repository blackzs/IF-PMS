/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.controller;

import com.alibaba.fastjson.JSON;
import com.xnck.mfpms.constant.ContextConstant;
import com.xnck.mfpms.util.ContextUtils;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 视图控制器基类
 * @author zhangmengliang
 *
 */
public class BaseController {

	private static Logger logger = Logger.getLogger(BaseController.class);
	
	/**
	 * 封装并以json返回成功执行的信息
	 * @param response
	 * @param data
	 * @param msg
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	protected void responseSuccessMsg(HttpServletResponse response, Object data, String msg){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put(ContextConstant.KEY_DATA, data);
			map.put(ContextConstant.KEY_CODE, ContextConstant.VALUE_SUCCESS);
			map.put(ContextConstant.KEY_MSG, msg);
			response.setContentType("application/json;charset=UTF-8");
			ContextUtils.responseString(response, JSON.toJSONString(map));
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}
	
	/**
	 * 封装并以json返回错误执行的信息
	 * @param response
	 * @param msg
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	protected void responseErrorMsg(HttpServletResponse response, String msg){
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put(ContextConstant.KEY_CODE, ContextConstant.VALUE_ERROR);
			map.put(ContextConstant.KEY_MSG, msg);
			response.setContentType("application/json;charset=UTF-8");
			ContextUtils.responseString(response, JSON.toJSONString(map));
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

}
