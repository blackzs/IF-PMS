/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.controller;

import com.xnck.mfpms.annotation.Permission;
import com.xnck.mfpms.entity.ResourceInfo;
import com.xnck.mfpms.entity.ResourceType;
import com.xnck.mfpms.exception.ValidateException;
import com.xnck.mfpms.service.ResService;
import com.xnck.mfpms.util.SessionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sys/pms/res")
public class SysResourceController extends BaseController{
    private static final Logger log = Logger.getLogger(SysResourceController.class);

    @Autowired
    private ResService resService;

    /**
     * 资源列表
     * @param request
     * @param response
     * @return
     */
    @Permission("url_sys_pms_res_list")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView getList(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mv = new ModelAndView("/sys/pms/res/list");
        return mv;
    }

    /**
     * 资源总数
     * @param request
     * @param response
     * @param resName
     */
    @Permission("url_sys_pms_res_list")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public void postList(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam(value = "name")String resName){
        try {
            int count = resService.getsCount(resName);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("count", count);
            responseSuccessMsg(response, map, "OK");
        }
        catch (Exception e){
            log.error(e.toString());
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 资源主体
     * @param request
     * @param response
     * @param pageSize
     * @param beginIndex
     * @param resName
     * @param orderName
     * @param orderType
     * @return
     */
    @Permission("url_sys_pms_res_listbody")
    @RequestMapping(value = "/listbody", method = RequestMethod.POST)
    public ModelAndView postBody(HttpServletRequest request, HttpServletResponse response,
                                 @RequestParam(value = "limit")int pageSize,
                                 @RequestParam(value = "offset")int beginIndex,
                                 @RequestParam(value = "name")String resName,
                                 @RequestParam(value = "sort", required = false)String orderName,
                                 @RequestParam(value = "order", required = false)String orderType){
        ModelAndView mv = new ModelAndView("/sys/pms/res/listbody");
        try {
            List<ResourceInfo> reses = resService.gets(resName, orderName, orderType, pageSize, beginIndex);
            mv.addObject("reses", reses);
        }
        catch (Exception e){
            log.error(e.toString());
        }
        return mv;
    }

    /**
     * 创建资源
     * @param request
     * @param response
     * @return
     */
    @Permission("url_sys_pms_res_create")
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView getCreate(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mv = new ModelAndView("/sys/pms/res/create");
        try {
            List<ResourceType> types = resService.getTypes();
            mv.addObject("types", types);
        }
        catch (Exception e){
            log.error(e);
        }
        return mv;
    }

    /**
     * 创建资源
     * @param request
     * @param response
     * @param displayName
     * @param code
     * @param typeId
     * @param viewName
     * @param remark
     * @param enable
     */
    @Permission("url_sys_pms_res_create")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void postCreate(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam(value = "displayname")String displayName,
                           @RequestParam(value = "code")String code,
                           @RequestParam(value = "type")String typeId,
                           @RequestParam(value = "viewname")String viewName,
                           @RequestParam(value = "remark")String remark,
                           @RequestParam(value = "enable", required = false)boolean enable){
        try {
            String curUserId = SessionUtils.getCurrentUserId(request);
            resService.create(curUserId, displayName, code, typeId, viewName, remark, enable);
            responseSuccessMsg(response, null, "添加成功");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 修改资源
     * @param request
     * @param response
     * @return
     */
    @Permission("url_sys_pms_res_update")
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView getUpdate(HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam(value = "id")String resId){
        ModelAndView mv = new ModelAndView("/sys/pms/res/update");
        try {
            List<ResourceType> types = resService.getTypes();
            mv.addObject("types", types);
            ResourceInfo res = resService.get(resId);
            mv.addObject("res", res);
        }
        catch (Exception e){
            log.error(e);
        }
        return mv;
    }

    /**
     * 修改资源
     * @param request
     * @param response
     * @param resId
     * @param displayName
     * @param code
     * @param typeId
     * @param viewName
     * @param remark
     * @param enable
     */
    @Permission("url_sys_pms_res_update")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void postUpdate(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam(value = "id")String resId,
                           @RequestParam(value = "displayname")String displayName,
                           @RequestParam(value = "code")String code,
                           @RequestParam(value = "type")String typeId,
                           @RequestParam(value = "viewname")String viewName,
                           @RequestParam(value = "remark")String remark,
                           @RequestParam(value = "enable", required = false)boolean enable){
        try {
            String curUserId = SessionUtils.getCurrentUserId(request);
            resService.update(resId, displayName, code, typeId, viewName, remark, enable);
            responseSuccessMsg(response, null, "修改成功");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 批量删除
     * @param request
     * @param response
     * @param resIds
     */
    @Permission("url_sys_pms_res_multidel")
    @RequestMapping(value = "/multidel", method = RequestMethod.POST)
    public void postMulti(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam(value = "ids")String resIds){
        try {
            resService.delete(resIds);
            responseSuccessMsg(response, null, "删除成功");
        }
        catch (Exception e){
            log.error(e.toString());
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 设置权限
     * @param request
     * @param response
     * @param resId
     * @return
     */
    @Permission("url_sys_pms_res_setaction")
    @RequestMapping(value = "/setaction", method = RequestMethod.GET)
    public ModelAndView getSetAction(HttpServletRequest request, HttpServletResponse response,
                                     @RequestParam(value = "id")String resId){
        ModelAndView mv = new ModelAndView("/sys/pms/res/setaction");
        try {
            ResourceInfo res = resService.get(resId);
            mv.addObject("res", res);
        }
        catch (Exception e){
            log.error(e);
        }
        return mv;
    }

    /**
     * 设置权限
     * @param request
     * @param response
     * @param resId
     * @param actionIdStr
     */
    @Permission("url_sys_pms_res_setaction")
    @RequestMapping(value = "/setaction", method = RequestMethod.POST)
    public void postSetAction(HttpServletRequest request, HttpServletResponse response,
                              @RequestParam(value = "id")String resId,
                              @RequestParam(value = "ids")String actionIdStr){
        try {
            resService.joinAction(resId, actionIdStr);
            responseSuccessMsg(response, null, "关联成功");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部异常");
        }
    }

    /**
     * 权限树
     * @param request
     * @param response
     * @param resId
     */
    @Permission("url_sys_pms_res_actionnodes")
    @RequestMapping(value = "/actionnodes", method = RequestMethod.POST)
    public void postActionNodes(HttpServletRequest request, HttpServletResponse response,
                                @RequestParam(value = "id")String resId){
        try {
            List<Map<String, Object>> nodes = resService.getActionNodes(resId);
            responseSuccessMsg(response, nodes, "ok");
        }
        catch (ValidateException ve){
            log.error(ve);
            responseErrorMsg(response, ve.getMessage());
        }
        catch (Exception e){
            log.error(e);
            responseErrorMsg(response, "内部错误");
        }
    }
}
