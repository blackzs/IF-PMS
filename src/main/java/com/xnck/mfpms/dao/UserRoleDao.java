/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.UserRole;
import org.springframework.stereotype.Component;

@Component
public class UserRoleDao extends BaseDao<UserRole>{

    public void insert(String userId, String roleId){
        UserRole userRole = new UserRole();
        userRole.setRoleid(roleId);
        userRole.setUserid(userId);
        this.dao.insert(userRole);
    }
}
