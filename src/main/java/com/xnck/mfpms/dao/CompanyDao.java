/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.CompanyInfo;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CompanyDao extends BaseDao<CompanyInfo>{

    /**
     * 新增企业信息
     * @param id
     * @param shortName
     * @param displayName
     * @param linkMan
     * @param email
     * @param tel
     * @param creatorId
     * @param createTime
     */
    public void insert(String id, String shortName, String displayName,
                       String linkMan, String email, String tel, String creatorId, Date createTime){
        CompanyInfo companyInfo = new CompanyInfo();
        companyInfo.setDisplayname(displayName);
        companyInfo.setEmail(email);
        companyInfo.setCreatetime(createTime);
        companyInfo.setId(id);
        companyInfo.setLinkman(linkMan);
        companyInfo.setShortname(shortName);
        companyInfo.setTel(tel);
        companyInfo.setCreatorid(creatorId);
        this.dao.insert(companyInfo);
    }
}
