/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.ActionInfo;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ActionDao extends BaseDao<ActionInfo>{

    public ActionInfo insert(String id, String displayName, String parentId, String remark, boolean enable,
                             Date addTime, String creatorId){
        ActionInfo action = new ActionInfo();
        action.setDisplayname(displayName);
        action.setId(id);
        action.setAddtime(addTime);
        action.setCreaterid(creatorId);
        action.setEnable(enable);
        action.setParentid(parentId);
        action.setRemark(remark);
        return this.dao.insert(action);
    }
}
