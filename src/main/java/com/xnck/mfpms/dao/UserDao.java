/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.UserInfo;
import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class UserDao extends BaseDao<UserInfo>{

    public UserInfo insert(String id, String companyId, String name, String displayName, String email, String creatorId, boolean enable, Date createTime){
        UserInfo userInfo = new UserInfo();
        userInfo.setAddtime(createTime);
        userInfo.setEmail(email);
        userInfo.setCreaterid(creatorId);
        userInfo.setDisplayname(displayName);
        userInfo.setEnable(enable);
        userInfo.setId(id);
        userInfo.setName(name);
        userInfo.setCompanyid(companyId);
        return this.dao.insert(userInfo);
    }

    /**
     * 分页获得用户(数据规则限制)
     * @param byUserId
     * @param cnd
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<UserInfo> getUsersByRule(String byUserId, Condition cnd, int currentPage, int pageSize){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getUsersByRule", byUserId);
        sql.setParam("byuserid", byUserId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得用户(数据规则限制)
     * @param byUserId
     * @param cnd
     * @return
     */
    public List<UserInfo> getUsersByRule(String byUserId, Condition cnd){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getUsersByRule", byUserId);
        sql.setParam("byuserid", byUserId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得用户总数量(数据规则限制)
     * @param byUserId 用来当做条件的用户ID
     * @param cnd 输入的查询条件
     * @return
     */
    public int getUsersCountByRule(String byUserId, Condition cnd){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getUsersCountByRule", byUserId);
        sql.setParam("byuserid", byUserId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得已关联角色用户
     * @param cnd
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<UserInfo> getJoinRole(String roleId, Condition cnd, int currentPage, int pageSize){
        Sql sql = this.dao.sqls().create("userDao.getJoinRole");
        sql.setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得已关联角色用户
     * @param cnd
     * @return
     */
    public List<UserInfo> getJoinRole(String roleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getJoinRole");
        sql.setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得已关联角色用户总数量
     * @param cnd 输入的查询条件
     * @return
     */
    public int getCountJoinRole(String roleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getCountJoinRole");
        sql.setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得未关联角色用户
     * @param cnd
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<UserInfo> getNotJoinRole(String roleId, Condition cnd, int currentPage, int pageSize){
        Sql sql = this.dao.sqls().create("userDao.getNotJoinRole");
        sql.setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得未关联角色用户
     * @param cnd
     * @return
     */
    public List<UserInfo> getNotJoinRole(String roleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getNotJoinRole");
        sql.setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得未关联角色用户总数量
     * @param cnd 输入的查询条件
     * @return
     */
    public int getCountNotJoinRole(String roleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getCountNotJoinRole");
        sql.setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得已关联角色用户(数据规则限制)
     * @param byUserId
     * @param cnd
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<UserInfo> getJoinRoleByRule(String byUserId, String roleId, Condition cnd, int currentPage, int pageSize){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getJoinRoleByRule", byUserId);
        sql.setParam("byuserid", byUserId).setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得已关联角色用户(数据规则限制)
     * @param byUserId
     * @param cnd
     * @return
     */
    public List<UserInfo> getJoinRoleByRule(String byUserId, String roleId, Condition cnd){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getJoinRoleByRule", byUserId);
        sql.setParam("byuserid", byUserId).setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得已关联角色用户总数量(数据规则限制)
     * @param byUserId 用来当做条件的用户ID
     * @param cnd 输入的查询条件
     * @return
     */
    public int getCountJoinRoleByRule(String byUserId, String roleId, Condition cnd){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getCountJoinRoleByRule", byUserId);
        sql.setParam("byuserid", byUserId).setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得未关联角色用户(数据规则限制)
     * @param byUserId
     * @param cnd
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<UserInfo> getNotJoinRoleByRule(String byUserId, String roleId, Condition cnd, int currentPage, int pageSize){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getNotJoinRoleByRule", byUserId);
        sql.setParam("byuserid", byUserId).setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得未关联角色用户(数据规则限制)
     * @param byUserId
     * @param cnd
     * @return
     */
    public List<UserInfo> getNotJoinRoleByRule(String byUserId, String roleId, Condition cnd){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getNotJoinRoleByRule", byUserId);
        sql.setParam("byuserid", byUserId).setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得未关联角色用户总数量(数据规则限制)
     * @param byUserId 用来当做条件的用户ID
     * @param cnd 输入的查询条件
     * @return
     */
    public int getCountNotJoinRoleByRule(String byUserId, String roleId, Condition cnd){
        Sql sql = SqlTool.injectSearchSql(this.dao, "userDao.getCountNotJoinRoleByRule", byUserId);
        sql.setParam("byuserid", byUserId).setParam("roleid", roleId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得已关联权限的用户
     * @return
     */
    public List<UserInfo> getJoinAction(String actionId, Condition cnd, int currentPage, int pageSize){
        Sql sql = this.dao.sqls().create("userDao.getJoinAction");
        sql.setParam("actionid", actionId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得已关联权限的用户
     * @return
     */
    public List<UserInfo> getJoinAction(String actionId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getJoinAction");
        sql.setParam("actionid", actionId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得已关联权限的用户总数量
     * @return
     */
    public int getCountJoinAction(String actionId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getCountJoinAction");
        sql.setParam("actionid", actionId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得未关联权限的用户
     * @return
     */
    public List<UserInfo> getNotJoinAction(String actionId, Condition cnd, int currentPage, int pageSize){
        Sql sql = this.dao.sqls().create("userDao.getNotJoinAction");
        sql.setParam("actionid", actionId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得未关联权限的用户
     * @return
     */
    public List<UserInfo> getNotJoinAction(String actionId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getNotJoinAction");
        sql.setParam("actionid", actionId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得未关联权限的用户总数量
     * @return
     */
    public int getCountNotJoinAction(String actionId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getCountNotJoinAction");
        sql.setParam("actionid", actionId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得已关联规则的用户
     * @return
     */
    public List<UserInfo> getJoinRule(String ruleId, Condition cnd, int currentPage, int pageSize){
        Sql sql = this.dao.sqls().create("userDao.getJoinRule");
        sql.setParam("ruleid", ruleId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得已关联规则的用户
     * @return
     */
    public List<UserInfo> getJoinRule(String ruleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getJoinRule");
        sql.setParam("ruleid", ruleId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得已关联规则的用户总数量
     * @return
     */
    public int getCountJoinRule(String ruleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getCountJoinRule");
        sql.setParam("ruleid", ruleId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }

    /**
     * 分页获得未关联规则的用户
     * @return
     */
    public List<UserInfo> getNotJoinRule(String ruleId, Condition cnd, int currentPage, int pageSize){
        Sql sql = this.dao.sqls().create("userDao.getNotJoinRule");
        sql.setParam("ruleid", ruleId);
        sql.setCondition(cnd);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 获得未关联规则的用户
     * @return
     */
    public List<UserInfo> getNotJoinRule(String ruleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getNotJoinRule");
        sql.setParam("ruleid", ruleId);
        sql.setCondition(cnd);
        return this.search(sql);
    }

    /**
     * 按条件获得未关联规则的用户总数量
     * @return
     */
    public int getCountNotJoinRule(String ruleId, Condition cnd){
        Sql sql = this.dao.sqls().create("userDao.getCountNotJoinRule");
        sql.setParam("ruleid", ruleId);
        sql.setCondition(cnd);
        return this.searchCount(sql);
    }
}
