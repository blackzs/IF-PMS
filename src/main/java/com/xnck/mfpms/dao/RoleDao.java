/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.RoleInfo;
import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class RoleDao extends BaseDao<RoleInfo>{

    public RoleInfo insert(String id, String displayName, String creatorId, boolean enable, Date addTime){
        RoleInfo role = new RoleInfo();
        role.setDisplayname(displayName);
        role.setEnable(enable);
        role.setCreaterid(creatorId);
        role.setAddtime(addTime);
        role.setId(id);
        return this.dao.insert(role);
    }

    /**
     * 人员已关联的角色数量
     * @param userId
     * @param condition
     * @return
     */
    public int getRolesCountJoinUser(String userId, Condition condition) {
        Sql sql = dao.sqls().create("roleDao.getRolesCountJoinUser");
        sql.params().set("userid", userId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 人员已关联的角色
     * @param userId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<RoleInfo> getRolesJoinUser(String userId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("roleDao.getRolesJoinUser");
        sql.params().set("userid", userId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 人员未关联的角色数量
     * @param userId
     * @param condition
     * @return
     */
    public int getRolesCountNotJoinUser(String userId, Condition condition) {
        Sql sql = dao.sqls().create("roleDao.getRolesCountNotJoinUser");
        sql.params().set("userid", userId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 人员已关联的角色
     * @param userId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<RoleInfo> getRolesNotJoinUser(String userId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("roleDao.getRolesNotJoinUser");
        sql.params().set("userid", userId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 权限已关联的角色数量
     * @param actionId
     * @param condition
     * @return
     */
    public int getCountJoinAction(String actionId, Condition condition) {
        Sql sql = dao.sqls().create("roleDao.getCountJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 权限已关联的角色
     * @param actionId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<RoleInfo> getJoinAction(String actionId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("roleDao.getJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 权限未关联的角色数量
     * @param actionId
     * @param condition
     * @return
     */
    public int getCountNotJoinAction(String actionId, Condition condition) {
        Sql sql = dao.sqls().create("roleDao.getCountNotJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 权限已关联的角色
     * @param actionId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<RoleInfo> getNotJoinAction(String actionId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("roleDao.getNotJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 规则已关联的角色数量
     * @param ruleId
     * @param condition
     * @return
     */
    public int getCountJoinRule(String ruleId, Condition condition) {
        Sql sql = dao.sqls().create("roleDao.getCountJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 规则已关联的角色
     * @param ruleId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<RoleInfo> getJoinRule(String ruleId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("roleDao.getJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 规则未关联的角色数量
     * @param ruleId
     * @param condition
     * @return
     */
    public int getCountNotJoinRule(String ruleId, Condition condition) {
        Sql sql = dao.sqls().create("roleDao.getCountNotJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 规则已关联的角色
     * @param ruleId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<RoleInfo> getNotJoinRule(String ruleId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("roleDao.getNotJoinRule");
        sql.params().set("ruleid", ruleId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }
}
