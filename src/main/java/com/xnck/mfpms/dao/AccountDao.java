/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.AccountInfo;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AccountDao extends BaseDao<AccountInfo>{

    public AccountInfo insert(String id, String loginName, String loginPwd, String userId, Date createTime){
        AccountInfo account = new AccountInfo();
        account.setCreatetime(createTime);
        account.setId(id);
        account.setLoginname(loginName);
        account.setLoginpwd(loginPwd);
        account.setUserid(userId);
        return this.dao.insert(account);
    }
}
