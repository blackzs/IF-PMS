/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.ResourceInfo;
import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class ResourceDao extends BaseDao<ResourceInfo>{

    public ResourceInfo insert(String id, String displayName, String code, String typeId,
                               String viewName, String remark, boolean enable, Date addTime, String creatorId){
        ResourceInfo res = new ResourceInfo();
        res.setEnable(enable);
        res.setId(id);
        res.setAddtime(addTime);
        res.setCode(code);
        res.setCreaterid(creatorId);
        res.setDisplayname(displayName);
        res.setRemark(remark);
        res.setTypeid(typeId);
        res.setViewname(viewName);
        return this.dao.insert(res);
    }

    /**
     * 获得人员所属角色对应的权限具有的资源
     * @param userId 人员ID
     * @param condition 查询条件
     * @return
     * @author:zhangmengliang
     * @date: 2015年11月1日
     */
    public List<ResourceInfo> getsForUserRoleAction(String userId, Condition condition){
        Sql sql = dao.sqls().create("resDao.getsForUserRoleAction");
        sql.params().set("userid", userId);
        sql.setCondition(condition);
        return this.search(sql);
    }

    /**
     * 获得人员对应的权限具有的资源
     * @param userId 人员ID
     * @param condition 查询条件
     * @return
     * @author:zhangmengliang
     * @date: 2015年11月1日
     */
    public List<ResourceInfo> getsForUserAction(String userId, Condition condition){
        Sql sql = dao.sqls().create("resDao.getsForUserAction");
        sql.params().set("userid", userId);
        sql.setCondition(condition);
        return this.search(sql);
    }

    /**
     * 权限已关联的资源数量
     * @param actionId
     * @param condition
     * @return
     */
    public int getCountJoinAction(String actionId, Condition condition) {
        Sql sql = dao.sqls().create("resDao.getCountJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 权限已关联的资源
     * @param actionId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<ResourceInfo> getJoinAction(String actionId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("resDao.getJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }

    /**
     * 权限未关联的资源数量
     * @param actionId
     * @param condition
     * @return
     */
    public int getCountNotJoinAction(String actionId, Condition condition) {
        Sql sql = dao.sqls().create("resDao.getCountNotJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchCount(sql);
    }

    /**
     * 权限未关联的资源
     * @param actionId
     * @param condition
     * @param currentPage
     * @param pageSize
     * @return
     */
    public List<ResourceInfo> getNotJoinAction(String actionId, Condition condition, int currentPage, int pageSize){
        Sql sql = dao.sqls().create("resDao.getNotJoinAction");
        sql.params().set("actionid", actionId);
        sql.setCondition(condition);
        return this.searchByPage(sql, currentPage, pageSize);
    }
}
