/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.dao;

import com.xnck.mfpms.entity.RuleInfo;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class RuleDao extends BaseDao<RuleInfo>{

    public RuleInfo insert(String id, String displayName, String parentId, boolean enable,
                           Date addTime, String creatorId){
        RuleInfo rule = new RuleInfo();
        rule.setDisplayname(displayName);
        rule.setId(id);
        rule.setAddtime(addTime);
        rule.setCreaterid(creatorId);
        rule.setEnable(enable);
        rule.setParentid(parentId);
        return this.dao.insert(rule);
    }

    
}
