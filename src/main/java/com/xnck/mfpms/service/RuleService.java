/*
 * Copyright (C),2015,北京新诺创科软件技术有限公司
 * author zhangmengliang
 */
package com.xnck.mfpms.service;

import com.xiaoleilu.hutool.DateUtil;
import com.xiaoleilu.hutool.StrUtil;
import com.xnck.mfpms.dao.*;
import com.xnck.mfpms.entity.*;
import com.xnck.mfpms.exception.ValidateException;
import com.xnck.mfpms.util.PagerUtil;
import com.xnck.mfpms.util.ValidatorUtils;
import org.nutz.dao.Cnd;
import org.nutz.dao.sql.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class RuleService {

    @Autowired
    private RuleDao ruleDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private QueryPartDao qpartDao;

    @Autowired
    private RoleRuleDao roleRuleDao;

    @Autowired
    private UserRuleDao userRuleDao;

    @Autowired
    private RuleChildDao ruleChildDao;

    @Autowired
    private RuleQPartDao ruleQPartDao;

    public RuleInfo get(String ruleId){
        return ruleDao.get(ruleId);
    }

    public int getsCount(String name, String parentId){
        Criteria cri = Cnd.cri();
        this.getSearchCnd(cri, name, parentId);
        return ruleDao.searchCount(cri);
    }

    public List<RuleInfo> gets(String name, String parentId, String orderName, String orderType,
                               int pageSize, int beginIndex){
        Criteria cri = Cnd.cri();
        this.getSearchCnd(cri, name, parentId);
        PagerUtil.getSearchOrder(cri, RuleInfo.FIELD_PARENTID, orderName, orderType);
        int currentPage = PagerUtil.getCurrentPage(beginIndex, pageSize);
        return ruleDao.searchByPage(cri, currentPage, pageSize);
    }

    public List<Map<String, Object>> getNodesByParentId(String parentId){
        List<RuleInfo> rules = ruleDao.search(Cnd.where(RuleInfo.FIELD_PARENTID, "=", parentId));
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        for (RuleInfo rule : rules) {
            Map<String, Object> child = new HashMap<String, Object>();
            child.put("id", rule.getId());
            if (rule.getChildcount() > 0) {
                child.put("isParent", true);
            }
            else {
                child.put("isParent", false);
            }
            child.put("name", rule.getDisplayname());
            result.add(child);
        }
        return result;
    }

    public List<Map<String, Object>> getAllNodes() throws Exception {
        List<RuleInfo> rules = ruleDao.search(Cnd.orderBy().asc(RuleInfo.FIELD_DISPLAYNAME));
        List<Map<String, Object>> nodes = new ArrayList<Map<String, Object>>();
        for (RuleInfo rule: rules) {
            Map<String, Object> node = new HashMap<String, Object>();
            node.put("id", rule.getId());
            node.put("name", rule.getDisplayname());
            node.put("pId", rule.getParentid());
            nodes.add(node);
        }
        return nodes;
    }

    /**
     * 创建规则
     * @param creatorId 创建者
     * @param displayName 显示名称
     * @param parentId 所属父级
     * @param enable 是否启用
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public void create(String creatorId, String displayName, String parentId, boolean enable) throws Exception {
        this.checkRuleInsertInput(creatorId, displayName, parentId);
        String ruleId = UUID.randomUUID().toString();
        Date addTime = DateUtil.date();
        RuleInfo rule = ruleDao.insert(ruleId, displayName, parentId, enable, addTime, creatorId);
        //写入父级追踪信息
        RuleInfo parentRule = ruleDao.get(parentId);
        if (null != parentRule){
            ruleDao.findLink(parentRule, RuleInfo.FIELD_ALL_PARENTS);
            rule.setAllparents(parentRule.getAllparents());
            rule.getAllparents().add(parentRule);
            ruleDao.insertRelation(rule, RuleInfo.FIELD_ALL_PARENTS);
        }
    }

    /**
     * 修改
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(String id, String displayName, String parentId, boolean enable) throws Exception {
        this.checkRuleUpdateInput(displayName, parentId);
        RuleInfo rule = ruleDao.get(id);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        ruleDao.findLink(rule, RuleInfo.FIELD_PARENT);
        if (null != rule.getParent() && !rule.getParent().isEnable()){
            throw new ValidateException("父规则处于禁用状态");
        }
        //清除父级追踪信息
        ruleDao.clearLinks(rule, RuleInfo.FIELD_ALL_PARENTS);
        //写入父级追踪信息
        RuleInfo parentRule = ruleDao.get(parentId);
        if (null != parentRule){
            ruleDao.findLink(parentRule, RuleInfo.FIELD_ALL_PARENTS);
            rule.setAllparents(parentRule.getAllparents());
            rule.getAllparents().add(parentRule);
            ruleDao.insertRelation(rule, RuleInfo.FIELD_ALL_PARENTS);
        }
        //禁用所有子规则
        if (!enable){
            ruleDao.findLink(rule, RuleInfo.FIELD_ALL_CHILDS);
            for (RuleInfo child: rule.getAllchilds()) {
                child.setEnable(enable);
                ruleDao.update(child);
            }
        }
        //更新规则
        rule.setParentid(parentId);
        rule.setDisplayname(displayName);
        rule.setEnable(enable);
        ruleDao.update(rule);
    }

    /**
     * 删除
     * @param ruleIds
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public void deletes(String ruleIds) throws Exception {
        int childCount = ruleChildDao.searchCount(Cnd.where(RuleChild.FIELD_PARENTID, "in", ruleIds.split(",")));
        if (childCount > 0){
            throw new ValidateException("包含子级规则，不可删除");
        }
        ruleDao.clear(Cnd.where(RuleInfo.FIELD_ID, "in", ruleIds.split(",")));
        ruleChildDao.clear(Cnd.where(RuleChild.FIELD_CHILDID, "in", ruleIds.split(",")));
        userRuleDao.clear(Cnd.where(UserRule.FIELD_RULEID, "in", ruleIds.split(",")));
        roleRuleDao.clear(Cnd.where(RoleRule.FIELD_RULEID, "in", ruleIds.split(",")));
        ruleQPartDao.clear(Cnd.where(RuleQueryPart.FIELD_RULEID, "in", ruleIds.split(",")));
    }

    /**
     * 已关联角色数量
     * @param ruleId
     * @param roleName
     * @return
     */
    public int getJoinedRolesCount(String ruleId, String roleName){
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(roleName)) {
            cri.where().andLike(RoleInfo.FIELD_DISPLAYNAME, roleName);
        }
        return roleDao.getCountJoinRule(ruleId, cri);
    }

    /**
     * 未关联角色数量
     * @param ruleId
     * @param roleName
     * @return
     */
    public int getNoJoinRolesCount(String ruleId, String roleName){
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(roleName)) {
            cri.where().andLike(RoleInfo.FIELD_DISPLAYNAME, roleName);
        }
        return roleDao.getCountNotJoinRule(ruleId, cri);
    }

    /**
     * 已关联角色
     * @return
     * @throws Exception
     */
    public List<RoleInfo> getJoinedRoles(String ruleId, String roleName,
                                         String orderName, String orderType,
                                         int pageSize, int beginIndex) {
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(roleName)) {
            cri.where().andLike(RoleInfo.FIELD_DISPLAYNAME, roleName);
        }
        PagerUtil.getSearchOrder(cri, RoleInfo.FIELD_DISPLAYNAME, orderName, orderType);
        int currentPage = PagerUtil.getCurrentPage(beginIndex, pageSize);
        return roleDao.getJoinRule(ruleId, cri, currentPage, pageSize);
    }

    /**
     * 未已关联角色
     * @return
     * @throws Exception
     */
    public List<RoleInfo> getNoJoinRoles(String ruleId, String roleName,
                                         String orderName, String orderType,
                                         int pageSize, int beginIndex) throws Exception {
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(roleName)) {
            cri.where().andLike(RoleInfo.FIELD_DISPLAYNAME, roleName);
        }
        PagerUtil.getSearchOrder(cri, RoleInfo.FIELD_DISPLAYNAME, orderName, orderType);
        int currentPage = PagerUtil.getCurrentPage(beginIndex, pageSize);
        return roleDao.getNotJoinRule(ruleId, cri, currentPage, pageSize);
    }

    /**
     * 关联角色
     * @param ruleId 被关联规则
     * @param roleIdStr 待关联角色列表(以逗号相隔)
     */
    @Transactional(rollbackFor = Exception.class)
    public void joinRole(String ruleId, String roleIdStr) throws Exception {
        if (StrUtil.isBlank(roleIdStr)){
            throw new ValidateException("角色不存在");
        }
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        String[] roleIds = roleIdStr.split(",");
        ruleDao.findLink(rule, RuleInfo.FIELD_ROLES);
        List<String> roleIdList = this.getRoleIdNotJoin(roleIds, rule.getRoles());
        if (roleIdList.size() > 0){
            List<RoleInfo> roles = roleDao.search(Cnd.where(RoleInfo.FIELD_ID, "in", roleIdList.toArray(new String[roleIdList.size()])));
            if (roles.size() > 0){
                rule.setRoles(roles);
                ruleDao.insertRelation(rule, RuleInfo.FIELD_ROLES);
            }
        }
    }

    /**
     * 取消关联角色
     * @param ruleId 被取消关联的规则
     * @param roleIdStr 被关联角色列表(以逗号相隔)
     */
    @Transactional(rollbackFor = Exception.class)
    public void cancelJoinRole(String ruleId, String roleIdStr) throws Exception {
        if (StrUtil.isBlank(roleIdStr)){
            throw new ValidateException("角色不存在");
        }
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        String[] roleIds = roleIdStr.split(",");
        ruleDao.findLink(rule, RuleInfo.FIELD_ROLES);
        List<String> roleIdList = this.getRoleIdJoin(roleIds, rule.getRoles());
        if (roleIdList.size() > 0){
            roleRuleDao.clear(Cnd.where(RoleRule.FIELD_RULEID, "=", ruleId)
                    .and(RoleRule.FIELD_ROLEID, "in", roleIdList.toArray(new String[roleIdList.size()])));
        }
    }

    /**
     * 已关联规则的人员数量
     * @return
     */
    public int getJoinedUsersCount(String ruleId, String userName){
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(userName)) {
            cri.where().andLike(UserInfo.FIELD_DISPLAYNAME, userName);
        }
        return userDao.getCountJoinRule(ruleId, cri);
    }

    /**
     * 未关联规则的人员数量
     * @return
     */
    public int getNoJoinUsersCount(String ruleId, String userName){
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(userName)) {
            cri.where().andLike(UserInfo.FIELD_DISPLAYNAME, userName);
        }
        return userDao.getCountNotJoinRule(ruleId, cri);
    }

    /**
     * 已关联人员
     * @return
     * @throws Exception
     */
    public List<UserInfo> getJoinedUsers(String ruleId, String userName,
                                         String orderName, String orderType,
                                         int pageSize, int beginIndex) throws Exception {
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(userName)) {
            cri.where().andLike(UserInfo.FIELD_DISPLAYNAME, userName);
        }
        PagerUtil.getSearchOrder(cri, UserInfo.FIELD_DISPLAYNAME, orderName, orderType);
        int currentPage = PagerUtil.getCurrentPage(beginIndex, pageSize);
        return userDao.getJoinRule(ruleId, cri, currentPage, pageSize);
    }

    /**
     * 未已关联人员
     * @return
     * @throws Exception
     */
    public List<UserInfo> getNoJoinUsers(String ruleId, String userName,
                                         String orderName, String orderType,
                                         int pageSize, int beginIndex) throws Exception {
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(userName)) {
            cri.where().andLike(UserInfo.FIELD_DISPLAYNAME, userName);
        }
        PagerUtil.getSearchOrder(cri, UserInfo.FIELD_DISPLAYNAME, orderName, orderType);
        int currentPage = PagerUtil.getCurrentPage(beginIndex, pageSize);
        return userDao.getNotJoinRule(ruleId, cri, currentPage, pageSize);
    }

    /**
     * 关联人员和规则
     * @param ruleId 被关联的规则
     * @param userIdStr 待关联人员列表(以逗号相隔)
     */
    @Transactional(rollbackFor = Exception.class)
    public void joinUser(String ruleId, String userIdStr) throws Exception {
        if (StrUtil.isBlank(userIdStr)){
            throw new ValidateException("用户不存在");
        }
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        List<UserInfo> joiningUsers = userDao.search(Cnd.where(UserInfo.FIELD_ID, "in", userIdStr.split(",")));
        if (joiningUsers.size() > 0){
            ruleDao.findLink(rule, RuleInfo.FIELD_USERS);
            List<UserInfo> users = this.getUserIdNotJoin(joiningUsers, rule.getUsers());
            if (users.size() > 0){
                rule.setUsers(users);
                ruleDao.insertRelation(rule, RuleInfo.FIELD_USERS);
            }
        }
    }

    /**
     * 取消关联人员和规则
     * @param ruleId 被关联的规则
     * @param userIdStr 被关联人员列表(以逗号相隔)
     */
    @Transactional(rollbackFor = Exception.class)
    public void cancelJoinUser(String ruleId, String userIdStr) throws Exception {
        if (StrUtil.isBlank(userIdStr)){
            throw new ValidateException("用户不存在");
        }
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        List<UserInfo> joinedUsers = userDao.search(Cnd.where(UserInfo.FIELD_ID, "in", userIdStr.split(",")));
        if (joinedUsers.size() > 0){
            List<String> joinedUserIds = new ArrayList<String>();
            for (UserInfo joinedUser : joinedUsers) {
                joinedUserIds.add(joinedUser.getId());
            }
            userRuleDao.clear(Cnd.where(UserRule.FIELD_USERID, "in", joinedUserIds.toArray(new String[joinedUserIds.size()]))
                    .and(UserRule.FIELD_RULEID, "=", ruleId));
        }
    }

    /**
     * 已关联权限的数据资源数量
     * @return
     */
    public int getJoinedQPartCount(String ruleId, String qpartName){
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(qpartName)) {
            cri.where().and(
                    Cnd.exps(QueryPart.FIELD_DISPLAYNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_PARTNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_QUERYNAME, "like", "%" + qpartName + "%"));
        }
        return qpartDao.getCountJoinRule(ruleId, cri);
    }

    /**
     * 未关联权限的数据资源数量
     * @return
     */
    public int getNoJoinQPartCount(String ruleId, String qpartName){
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(qpartName)) {
            cri.where().and(
                    Cnd.exps(QueryPart.FIELD_DISPLAYNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_PARTNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_QUERYNAME, "like", "%" + qpartName + "%"));
        }
        return qpartDao.getCountNotJoinRule(ruleId, cri);
    }

    /**
     * 已关联数据资源
     * @return
     * @throws Exception
     */
    public List<QueryPart> getJoinedQPart(String ruleId, String qpartName,
                                           String orderName, String orderType,
                                           int pageSize, int beginIndex) throws Exception {
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(qpartName)) {
            cri.where().and(
                    Cnd.exps(QueryPart.FIELD_DISPLAYNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_PARTNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_QUERYNAME, "like", "%" + qpartName + "%"));
        }
        PagerUtil.getSearchOrder(cri, QueryPart.FIELD_DISPLAYNAME, orderName, orderType);
        int currentPage = PagerUtil.getCurrentPage(beginIndex, pageSize);
        return qpartDao.getJoinRule(ruleId, cri, currentPage, pageSize);
    }

    /**
     * 未已关联数据资源
     * @return
     * @throws Exception
     */
    public List<QueryPart> getNoJoinQPart(String ruleId, String qpartName,
                                           String orderName, String orderType,
                                           int pageSize, int beginIndex) throws Exception {
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        Criteria cri = Cnd.cri();
        if (StrUtil.isNotBlank(qpartName)) {
            cri.where().and(
                    Cnd.exps(QueryPart.FIELD_DISPLAYNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_PARTNAME, "like", "%" + qpartName + "%")
                            .or(QueryPart.FIELD_QUERYNAME, "like", "%" + qpartName + "%"));
        }
        PagerUtil.getSearchOrder(cri, QueryPart.FIELD_DISPLAYNAME, orderName, orderType);
        int currentPage = PagerUtil.getCurrentPage(beginIndex, pageSize);
        return qpartDao.getNotJoinRule(ruleId, cri, currentPage, pageSize);
    }

    /**
     * 关联规则和数据资源
     * @param ruleId 被关联的规则
     * @param qpartIdStr 待关联资源列表(以逗号相隔)
     */
    @Transactional(rollbackFor = Exception.class)
    public void joinQPart(String ruleId, String qpartIdStr) throws Exception {
        if (StrUtil.isBlank(qpartIdStr)){
            throw new ValidateException("资源不存在");
        }
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        List<QueryPart> joiningQParts = qpartDao.search(Cnd.where(QueryPart.FIELD_ID, "in", qpartIdStr.split(",")));
        if (joiningQParts.size() > 0){
            ruleDao.findLink(rule, RuleInfo.FIELD_QPARTS);
            List<QueryPart> qparts = this.getQPartNotJoin(joiningQParts, rule.getQparts());
            if (qparts.size() > 0){
                rule.setQparts(qparts);
                ruleDao.insertRelation(rule, RuleInfo.FIELD_QPARTS);
            }
        }
    }

    /**
     * 取消关联规则和数据资源
     * @param ruleId 被关联的规则
     * @param qpartIdStr 被关联数据资源列表(以逗号相隔)
     */
    @Transactional(rollbackFor = Exception.class)
    public void cancelJoinQPart(String ruleId, String qpartIdStr) throws Exception {
        if (StrUtil.isBlank(qpartIdStr)){
            throw new ValidateException("资源不存在");
        }
        RuleInfo rule = ruleDao.get(ruleId);
        if (null == rule){
            throw new ValidateException("规则不存在");
        }
        List<QueryPart> joinedQParts = qpartDao.search(Cnd.where(QueryPart.FIELD_ID, "in", qpartIdStr.split(",")));
        if (joinedQParts.size() > 0){
            List<String> joinedQPartIds = new ArrayList<String>();
            for (QueryPart joinedQPart : joinedQParts) {
                joinedQPartIds.add(joinedQPart.getId());
            }
            ruleQPartDao.clear(Cnd.where(RuleQueryPart.FIELD_QUERYPARTID, "in", joinedQPartIds.toArray(new String[joinedQPartIds.size()]))
                    .and(RuleQueryPart.FIELD_RULEID, "=", ruleId));
        }
    }

    private void getSearchCnd(Criteria cri, String name, String parentId){
        if (StrUtil.isNotBlank(name)){
            cri.where().and(
                    Cnd.exps(RuleInfo.FIELD_DISPLAYNAME, "like", "%" + name + "%"));
        }
        if (StrUtil.isNotBlank(parentId) && !"-1".equals(parentId)){
            cri.where().and(RuleInfo.FIELD_PARENTID, "=", parentId);
        }
    }

    /**
     * 获得传入的ID中未关联的数据资源
     */
    private List<QueryPart> getQPartNotJoin(List<QueryPart> joiningQParts, List<QueryPart> joinedQParts){
        List<QueryPart> QParts = new ArrayList<QueryPart>();
        for (QueryPart joiningQPart : joiningQParts) {
            boolean isIn = false;
            for (QueryPart joinedQPart : joinedQParts) {
                if (joinedQPart.getId().equals(joiningQPart.getId())){
                    isIn = true;
                    break;
                }
            }
            if (!isIn){
                QParts.add(joiningQPart);
            }
        }
        return QParts;
    }

    /**
     * 获得传入的ID中未关联的角色
     * @param roleIds
     * @param joinRoles
     * @return
     */
    private List<String> getRoleIdNotJoin(String[] roleIds, List<RoleInfo> joinRoles){
        List<String> roleIdList = new ArrayList<String>();
        for (String roleId : roleIds) {
            boolean isIn = false;
            for (RoleInfo role : joinRoles) {
                if (role.getId().equals(roleId)){
                    isIn = true;
                    break;
                }
            }
            if (!isIn){
                roleIdList.add(roleId);
            }
        }
        return roleIdList;
    }

    /**
     * 获得传入的ID中已关联的角色
     * @param roleIds
     * @param joinRoles
     * @return
     */
    private List<String> getRoleIdJoin(String[] roleIds, List<RoleInfo> joinRoles){
        List<String> roleIdList = new ArrayList<String>();
        for (String roleId : roleIds) {
            boolean isIn = false;
            for (RoleInfo role : joinRoles) {
                if (role.getId().equals(roleId)){
                    isIn = true;
                    break;
                }
            }
            if (isIn){
                roleIdList.add(roleId);
            }
        }
        return roleIdList;
    }

    /**
     * 获得传入的ID中未关联的人员
     */
    private List<UserInfo> getUserIdNotJoin(List<UserInfo> joiningUsers, List<UserInfo> joinedUsers){
        List<UserInfo> users = new ArrayList<UserInfo>();
        for (UserInfo joiningUser : joiningUsers) {
            boolean isIn = false;
            for (UserInfo joinedUser : joinedUsers) {
                if (joinedUser.getId().equals(joiningUser.getId())){
                    isIn = true;
                    break;
                }
            }
            if (!isIn){
                users.add(joiningUser);
            }
        }
        return users;
    }

    private void checkRuleInsertInput(String creatorId, String displayName, String parentId) throws Exception {
        if (ValidatorUtils.isEmpty(displayName)){
            throw new ValidateException("名称不能为空");
        }
        if(displayName.getBytes().length < 4 || displayName.getBytes().length > 50){
            throw new ValidateException("名称应由4-50个字符组成");
        }
        if (ValidatorUtils.isEmpty(parentId)){
            throw new ValidateException("所属父级不能为空");
        }
        if (ValidatorUtils.isEmpty(creatorId)){
            throw new ValidateException("创建者不能为空");
        }
    }

    private void checkRuleUpdateInput(String displayName, String parentId) throws Exception {
        if (ValidatorUtils.isEmpty(displayName)){
            throw new ValidateException("名称不能为空");
        }
        if(displayName.getBytes().length < 4 || displayName.getBytes().length > 50){
            throw new ValidateException("名称应由4-50个字符组成");
        }
        if (ValidatorUtils.isEmpty(parentId)){
            throw new ValidateException("所属父级不能为空");
        }
    }
}
